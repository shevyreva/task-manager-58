package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataLoadJsonFasterXmlRequest extends AbstractUserRequest {

    public DataLoadJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
