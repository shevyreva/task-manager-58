package ru.t1.shevyreva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserUpdateProfileRequest extends AbstractUserRequest {

    @Nullable String lastName;
    @Nullable
    private String firstName;
    @Nullable
    private String middleName;

    public UserUpdateProfileRequest(@Nullable final String token,
                                    @Nullable final String firstName,
                                    @Nullable final String middleName,
                                    @Nullable final String lastName) {
        super(token);
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

}
