package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.endpoint.*;

@Service
public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}
