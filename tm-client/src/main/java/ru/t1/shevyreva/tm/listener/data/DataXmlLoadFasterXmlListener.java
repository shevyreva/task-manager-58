package ru.t1.shevyreva.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataLoadXmlFasterXmlRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load Data to xml file.";

    @NotNull
    private final String NAME = "data-load-xml";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataLoadXmlFasterXmlRequest request = new DataLoadXmlFasterXmlRequest(getToken());
        domainEndpoint.loadXmlDataFasterXml(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
