package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

@Service
public interface ITokenService {

    @NotNull
    String getToken();

    @NotNull
    void setToken(@Nullable final String token);

}
