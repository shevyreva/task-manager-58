package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    private final String DESCRIPTION = "Close application.";

    @NotNull
    private final String NAME = "exit";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.exit(0);
    }

}
