package ru.t1.shevyreva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.event.ConsoleEvent;
import ru.t1.shevyreva.tm.listener.AbstractListener;

@Component
public class CommandListListener extends AbstractSystemListener {

    @NotNull
    private final String DESCRIPTION = "Show command list.";

    @NotNull
    private final String NAME = "commands";

    @NotNull
    private final String ARGUMENT = "-cmd";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) continue;
            @Nullable final String name = listener.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

}
