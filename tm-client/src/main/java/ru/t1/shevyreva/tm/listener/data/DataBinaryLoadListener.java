package ru.t1.shevyreva.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.dto.request.data.DataLoadBinaryRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.event.ConsoleEvent;

@Component
public class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-bin";
    @NotNull
    private final String DESCRIPTION = "Load Data from binary file.";

    @Override
    public @Nullable String getName() {
        return NAME;
    }

    @Override
    public @Nullable String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBinaryLoadListener.getName() == #consoleEvent.name")
    public @Nullable void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final DataLoadBinaryRequest request = new DataLoadBinaryRequest(getToken());
        domainEndpoint.loadDataBinary(request);
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
