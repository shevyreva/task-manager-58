package ru.t1.shevyreva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.shevyreva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.shevyreva.tm.listener.AbstractListener;
import ru.t1.shevyreva.tm.dto.model.ProjectDTO;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    public IProjectEndpoint projectEndpoint;

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[PROJECT ID]: " + project.getId());
        System.out.println("[PROJECT NAME]: " + project.getName());
        System.out.println("[PROJECT DESCRIPTION]: " + project.getDescription());
        System.out.println("[PROJECT STATUS]: " + Status.toName(project.getStatus()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
