package ru.t1.shevyreva.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.endpoint.IEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.UserNotFoundException;
import ru.t1.shevyreva.tm.exception.system.RequestEmptyException;
import ru.t1.shevyreva.tm.exception.user.AccessDeniedException;
import ru.t1.shevyreva.tm.exception.user.RoleEmptyException;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint implements IEndpoint {
    @Getter
    @NotNull
    @Autowired
    private IServiceLocator serviceLocator;

    /*public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }*/

    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new RequestEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new ModelNotFoundException();
        @NotNull final IServiceLocator serviceLocator = getServiceLocator();
        @NotNull final IUserDtoService userService = serviceLocator.getUserService();
        @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final UserDTO user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @Nullable final Role roleUser = user.getRole();
        if (roleUser != role) throw new AccessDeniedException();
        return serviceLocator.getAuthService().validateToken(token);
    }

}
