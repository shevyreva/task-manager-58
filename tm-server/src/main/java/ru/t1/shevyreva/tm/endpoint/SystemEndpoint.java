package ru.t1.shevyreva.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.dto.request.system.ServerAboutRequest;
import ru.t1.shevyreva.tm.dto.request.system.ServerVersionRequest;
import ru.t1.shevyreva.tm.dto.response.system.ServerAboutResponse;
import ru.t1.shevyreva.tm.dto.response.system.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint")
public class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @Autowired
    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        //response.setEmail("tratata@ya.ru");// for test
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}
