package ru.t1.shevyreva.tm.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.shevyreva.tm.api.DBConstants;
import ru.t1.shevyreva.tm.api.repository.model.IAbstractModelRepository;
import ru.t1.shevyreva.tm.comporator.DateComparator;
import ru.t1.shevyreva.tm.comporator.StatusComparator;
import ru.t1.shevyreva.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;

@Getter
@Repository
@Scope("prototype")
@AllArgsConstructor
@NoArgsConstructor
public abstract class AbstractModelRepository<M extends AbstractModel> implements IAbstractModelRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == DateComparator.INSTANCE) return DBConstants.COLUMN_CREATED;
        if (comparator == StatusComparator.INSTANCE) return DBConstants.COLUMN_STATUS;
        else return DBConstants.COLUMN_NAME;
    }

    @Override
    public void add(@NotNull M model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull M model) {
        entityManager.merge(model);
    }

}
