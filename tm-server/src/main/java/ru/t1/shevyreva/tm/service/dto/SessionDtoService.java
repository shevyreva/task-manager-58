package ru.t1.shevyreva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.shevyreva.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.dto.ISessionDtoService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.IdEmptyException;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    public ISessionDtoRepository getRepository(){
        return context.getBean(ISessionDtoRepository.class);
    }

    @NotNull
    @SneakyThrows
    public SessionDTO add(
            @Nullable final String userId,
            @Nullable final SessionDTO session
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();


        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            session.setUserId(userId);
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO add(
            @Nullable final SessionDTO session
    ) {
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            @Nullable final SessionDTO session = findOneById(userId, id);
            if (session == null) throw new TaskNotFoundException();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO removeOne(@Nullable final SessionDTO session) {
        if (session == null) throw new ModelNotFoundException();

        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.removeOne(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public SessionDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            SessionDTO session = findOneById(userId, id);
            sessionRepository.removeOneById(id);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public List<SessionDTO> findAll() {
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            return sessionRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<SessionDTO> add(@NotNull final Collection<SessionDTO> models) {
        if (models == null) throw new ModelNotFoundException();

        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            for (@NotNull SessionDTO session : models) {
                sessionRepository.add(session);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            sessionRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<SessionDTO> set(@NotNull final Collection<SessionDTO> models) {
        @Nullable final Collection<SessionDTO> entities;
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            removeAll();
            entities = add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String id) {
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            return (sessionRepository.findOneById(id) != null);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    public long getSize() {
        @NotNull final ISessionDtoRepository sessionRepository = getRepository();
        @NotNull final EntityManager entityManager = sessionRepository.getEntityManager();
        try {
            @Nullable final long count = sessionRepository.getSize();
            return count;
        } finally {
            entityManager.close();
        }
    }

}
