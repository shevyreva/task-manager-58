package ru.t1.shevyreva.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.shevyreva.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.port']}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['author.name']}")
    private String authorName;

    @NotNull
    @Value("#{environment['author.name']}")
    private String authorEmail;

    @NotNull
    @Value("#{environment['server.host']}")
    private String serverHost;

    @NotNull
    @Value("#{environment['application.version']}")
    private String applicationVersion;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUsername;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databasePassword;

    @NotNull
    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String HBM2DDL;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String ShowSQL;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String UseSecondLevelCache;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String UseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String UseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String CacheRegionPrefix;

    @NotNull
    @Value("#{environment['database.cache.region_factory_class']}")
    private String CacheRegionFactory;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String CacheProviderConfig;

}
