package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionDtoRepository extends IAbstractUserOwnedModelDtoRepository<SessionDTO> {

    void removeOne(@NotNull final SessionDTO session);

    @Override
    void clearForUser(@NotNull String userId);

    @Override
    void removeOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    List<SessionDTO> findAllForUser(@NotNull String userId);

    @Override
    SessionDTO findOneByIdForUser(@NotNull String userId, @NotNull String id);

    @Override
    long getSizeForUser(@NotNull String userId);

    @Override
    void clear();

    @Override
    @Nullable List<SessionDTO> findAll();

    @Override
    @Nullable SessionDTO findOneById(@NotNull String id);

    @Override
    void removeOneById(@NotNull String id);

    @Override
    long getSize();

}
