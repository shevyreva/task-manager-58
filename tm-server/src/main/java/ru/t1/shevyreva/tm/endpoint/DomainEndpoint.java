package ru.t1.shevyreva.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.shevyreva.tm.api.endpoint.IDomainEndpoint;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.dto.request.data.*;
import ru.t1.shevyreva.tm.dto.response.data.*;
import ru.t1.shevyreva.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @Autowired
    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @WebMethod
    public DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @Override
    @WebMethod
    public DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @Override
    @WebMethod
    public DataLoadBase64Response loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBase64Request request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @Override
    @WebMethod
    public DataSaveBase64Response saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBase64Request request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @Override
    @WebMethod
    public DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @Override
    @WebMethod
    public DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @Override
    @WebMethod
    public DataLoadJsonFasterXmlResponse loadJsonDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadJsonDataFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @Override
    @WebMethod
    public DataLoadJsonJaxBResponse loadJsonDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadJsonDataJaxB();
        return new DataLoadJsonJaxBResponse();
    }

    @Override
    @WebMethod
    public DataSaveJsonFasterXmlResponse saveJsonDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveJsonDataFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @Override
    @WebMethod
    public DataSaveJsonJaxBResponse saveJsonDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveJsonJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveJsonDataJaxB();
        return new DataSaveJsonJaxBResponse();
    }

    @Override
    @WebMethod
    public DataLoadXmlFasterXmlResponse loadXmlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadXmlDataFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @Override
    @WebMethod
    public DataLoadXmlJaxBResponse loadXmlDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadXmlDataJaxB();
        return new DataLoadXmlJaxBResponse();
    }

    @Override
    @WebMethod
    public DataSaveXmlFasterXmlResponse saveXmlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveXmlDataFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @Override
    @WebMethod
    public DataSaveXmlJaxBResponse saveXmlDataJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveXmlJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveXmlDataJaxB();
        return new DataSaveXmlJaxBResponse();
    }

    @Override
    @WebMethod
    public DataLoadYamlFasterXmlResponse loadYamlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataLoadYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadYamlDataFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @Override
    @WebMethod
    public DataSaveYamlFasterXmlResponse saveYamlDataFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataSaveYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveYamlDataFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }

}
