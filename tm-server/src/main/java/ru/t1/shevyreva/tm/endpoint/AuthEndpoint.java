package ru.t1.shevyreva.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.shevyreva.tm.api.service.IAuthService;
import ru.t1.shevyreva.tm.api.service.IServiceLocator;
import ru.t1.shevyreva.tm.api.service.dto.IUserDtoService;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;
import ru.t1.shevyreva.tm.dto.model.UserDTO;
import ru.t1.shevyreva.tm.dto.request.user.UserLoginRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.shevyreva.tm.dto.request.user.UserProfileRequest;
import ru.t1.shevyreva.tm.dto.response.user.UserLoginResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserLogoutResponse;
import ru.t1.shevyreva.tm.dto.response.user.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.shevyreva.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    /*@Autowired
    public AuthEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }*/

    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        authService.invalidate(session);
        return new UserLogoutResponse();
    }

    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request) {
        @NotNull SessionDTO session = check(request);
        @NotNull final IUserDtoService userService = getServiceLocator().getUserService();
        @NotNull final UserDTO user = userService.findOneById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
