package ru.t1.shevyreva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

public interface IAbstractUserOwnedModelDtoRepository<M extends AbstractUserOwnedModelDTO> extends IAbstractModelDtoRepository<M> {

    void addForUser(@NotNull final String userId, @NotNull final M model);

    void updateForUser(@NotNull final String userId, @NotNull final M model);

    void clearForUser(@NotNull final String userId);

    void removeOneByIdForUser(@NotNull final String userId, @NotNull final String id);

    List<M> findAllForUser(@NotNull final String userId);

    M findOneByIdForUser(@NotNull final String userId, @NotNull final String id);

    long getSizeForUser(@NotNull final String userId);

}


